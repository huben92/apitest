const express   = require('express'),
  app           = express(),
  bodyParser    = require('body-parser'),
  cors          = require('cors'),
  config        = require('./api/config/config'),
  http          = require('http').Server(app),
  path          = require('path'),
  jwt           = require('jsonwebtoken'),
  util          = require('util');

app.use(cors());
app.use('/media', express.static('media'));
app.use('/documents', express.static('documents'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Display welcome message
app.get('/', function(req, res){
  res.sendFile(path.join(__dirname+'/dst/index.html'));
});

/**
 * Add router to the app
 * @author malik firmansyah (imagixasia)
 *
 */
const routes = require('./api/router/v1/router');
routes(app, '/api/v1/'); // Register router

http.listen(config.port, function(){
  console.log('Listening on '+ config.port +'...');
});
