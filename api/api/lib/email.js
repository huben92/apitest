'use strict';

const config   = require('../config/config'),
  mailgun      = require('mailgun-js'),
  fs           = require('fs');

// var mg      = mailgun.client({username: 'api', key: process.env.MAILGUN_API_KEY || config.mailgunKey});

exports.validate = (email) => {
  const emailRX  = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return emailRX.test(email);
}

exports.send = (subject, message, to, from, next) => {
  console.log(`Sending email to ${to}`);
  const mg = mailgun({apiKey: process.env.MAILGUN_API_KEY || config.mailgunKey, domain: config.mailgunDomain});
  const d = new Date();
  from = (from) ? from : config.appName +' <'+ config.senderEmail +'@'+ config.mailgunDomain +'>';
  var html = '<div style="padding: 30px 0; background: #f3f5f9;">'
    + '<div style="background: #fff; border-radius: 3px; overflow: hidden; max-width: 640px; '
    + 'margin: 0 auto; box-shadow: 2px 2px 2px rgba(0,0,0,.1); '
    + 'font-family: -apple-system,BlinkMacSystemFont,\'Segoe UI\',Roboto,\'Helvetica Neue\','
    + 'Arial,sans-serif,\'Apple Color Emoji\',\'Segoe UI Emoji\',\'Segoe UI Symbol\'; color: #38328c;">'
    + '<div><img alt="header" id="email_header" src="cid:email_header.jpg" style="width: 100%; height: auto;"></div>'
    + '<div style="padding: 30px 60px 0; line-height: 1.6; font-size: 15px;">'
    + message +'</div>'

    + '<div style="background-color: #f3f3f3; margin: 60px 60px 30px; text-align: center; padding: 26px 0 12px 0;">'
    + '<div style="margin: 0 40px 10px; font-size: 14px;">'
    + '<b>Team Up For Impact</b> adalah kolaborasi organisasi, inisiatif dan individu yang mengajak masyarakat '
    + 'untuk berbuat sesuatu yang berdampak bagi bumi. Mari bergabung!.</div>'
    + '<a href="http://www.kemitraan.or.id/"><img alt="kemitraan" id="kemitraan" src="cid:logo_kemitraan.jpg" style="width: auto; height: 86px;"></a>'
    + '<a href="https://www.walhi.or.id/"><img alt="walhi" id="walhi" src="cid:logo_walhi.jpg" style="width: auto; height: 86px;"></a>'
    + '<a href="https://www.kabupatenlestari.org/"><img alt="ltkl" id="ltkl" src="cid:logo_ltkl.jpg" style="width: auto; height: 86px;"></a>'
    + '<a href="https://madaniberkelanjutan.id/"><img alt="madani" id="madani" src="cid:logo_madani.jpg" style="width: auto; height: 86px;"></a>'
    + '<div></div>'
    + '<a href="http://www.cerah.or.id/en"><img alt="cerah" id="cerah" src="cid:logo_cerah.jpg" style="width: auto; height: 76px;"></a>'
    // + '<a href="https://icel.or.id/"><img alt="icel" id="icel" src="cid:logo_icel.jpg" style="width: auto; height: 76px;"></a>'
    + '<a href="https://coaction.id/"><img alt="coaction" id="coaction" src="cid:logo_coaction.jpg" style="width: auto; height: 76px;"></a>'
    + '<a href="https://trendasia.org/"><img alt="trend-asia" id="trend-asia" src="cid:logo_ta.jpg" style="width: auto; height: 76px;"></a>'
    + '<div></div>'
    + '<a href="https://iesr.or.id/"><img alt="iesr" id="iesr" src="cid:logo_iesr.jpg" style="width: auto; height: 76px;"></a>'
    + '<a href="https://iesr.or.id/"><img alt="generasi-lestari" id="generasi-lestari" src="cid:logo_generasi_lestari.jpg" style="width: auto; height: 76px;"></a>'
    + '</div>'

    + '<div style="background-color: #38328c; padding: 20px 40px 34px;">'
    + '<table border="0"><tr><td style="width: 200px;"><img alt="tufi" id="tufi" src="cid:logo_tfi.png" style="width: auto; height: 100px;"></td>'
    + '<td style="vertical-align: top; padding-top: 22px;"><div style="color: #9b99c6; font-size: 15px; font-weight: bold; margin-bottom: 2px;">HUBUNGI KAMI</div>'
    + '<a href="mailto:contact@teamforimpact.org" style="text-decoration: none; color: #fff;">contact@teamforimpact.org</a></td></tr></table>'
    + '</div>'

    + '</div></div>';

  const mgCnf = {
    from: from,
    to: [to],
    subject: subject,
    text: message,
    html: html,
    inline: ['./assets/email_header.jpg', './assets/logo_tfi.png', './assets/logo_kemitraan.jpg',
      './assets/logo_ltkl.jpg', './assets/logo_madani.jpg', './assets/logo_walhi.jpg',
      './assets/logo_cerah.jpg', './assets/logo_iesr.jpg', './assets/logo_generasi_lestari.jpg',
      // './assets/logo_icel.jpg',
      './assets/logo_coaction.jpg', './assets/logo_ta.jpg'],
  }

  mg.messages().send(mgCnf, (err, body) => {
    if (err) {
      console.log('Sending email failed.');
      console.log(err);
    }
    console.log('Email delivered.');
    if (next && typeof next === 'function') next(err, body);
  })
}