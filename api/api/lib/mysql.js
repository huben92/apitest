'use strict';

var mysql = require('mysql'),
    async = require('async'),
    conf  = require('../config/database');

let xpool = null, xselect = '*', xlimit = 0, xpage = 0, xorder = '';

var mysqlEscape = (str) => {
  if ( typeof str != 'string' ) return str;
  return str.replace(/[\0\x08\x09\x1a\n\r"'\\]/g, function (char) {
  // return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
      switch (char) {
          case "\0":
              return "\\0";
          case "\x08":
              return "\\b";
          case "\x09":
              return "\\t";
          case "\x1a":
              return "\\z";
          case "\n":
              return "\\n";
          case "\r":
              return "\\r";
          case "\"":
          case "'":
          case "\\":
          // case "%":
              return "\\"+char; // prepends a backslash to backslash, percent,
                                // and double/single quotes
      }
  });
}

exports.escape = (str) => {
  return mysqlEscape(str);
}

exports.connect = (done) => {
  xpool = mysql.createPool(conf.mysql);
  xpool.getConnection(done);
};

exports.pool = () => {
  return xpool
};

exports.select = (fields) => {
  xselect = fields;
}

exports.order = (_order) => {
  if ( _order && _order != '' )
    xorder = 'ORDER BY '+ _order;
}

exports.limit = (limit, page) => {
  xlimit = parseInt(limit);
  if ( page ) xpage = parseInt(page);
}

exports.query = (query, done) => {
  xpool.query(query, done);
}

exports.get = (table, where, done) => {
  if (where && where.qo && where.qo.order) where.qo.order = `ORDER BY ${where.qo.order}`;
  const qo = (where && where.qo) ? where.qo : {
    select: xselect,
    limit: xlimit,
    page: xpage,
    order: xorder,
  };
  if (where && where.qo) delete where.qo;
  if (!qo.select) qo.select = '*';
  if (!qo.limit) qo.limit = 0;
  if (!qo.page) qo.page = 0;
  if (!qo.order) qo.order = '';
  xselect = '*'; xlimit = 0; xpage = 0; xorder = '';
  if (!xpool) return done(new Error('Missing database connection.'))

  var limit = '';
  if ( qo.limit > 0 )
    limit = ' LIMIT '+ qo.limit * qo.page +','+ qo.limit;

  if (where){
    var keys = Object.keys(where),
      whereClause = keys.map(function(key) { 
        if ( where[key] === null )
          return '`'+ key +'` is null';
        else if ( key.indexOf('!=') > 0 || key.indexOf('LIKE') > 0 )
          return key +' \''+ where[key] +'\'';
        else
          return '`'+ key +'` = \''+ where[key] +'\'';
      });

    xpool.query('SELECT '+ qo.select +' FROM `'+ table +'` WHERE '+ whereClause.join(' AND ') +' '+ qo.order +' '+ limit, function(err, rows){
      done(err, rows);
    });
  } else {
    xpool.query('SELECT '+ qo.select +' FROM `'+ table +'` '+ qo.order +' '+ limit, function(err, rows){
      done(err, rows);
    });
  }
}

exports.count = (table, where, done) => {
  const qo = {
    select: xselect,
    limit: xlimit,
    page: xpage,
    order: xorder,
  };
  xselect = '*'; xlimit = 0; xpage = 0; xorder = '';
  if (!xpool) return done(new Error('Missing database connection.'))

  var limit = '';
  if ( qo.limit > 0 )
    limit = ' LIMIT '+ qo.limit * xpage +','+ qo.limit;

  if (where){
    var keys = Object.keys(where),
      whereClause = keys.map((key) => {
        if ( key.indexOf('!=') > 0 || key.indexOf('LIKE') > 0 )
          return key +' \''+ where[key] +'\'';
        else
          return '`'+ key +'` = \''+ where[key] +'\'';
      });

    xpool.query('SELECT COUNT(id) AS cid FROM `'+ table +'` WHERE '+ whereClause.join(' AND ') +' '+ qo.order +' '+ limit, (err, rows)=> {
      done(err, rows[0].cid);
    });
  } else {
    xpool.query('SELECT COUNT(id) AS cid FROM `'+ table +'` '+ qo.order +' '+ limit, (err, rows)=> {
      done(err, rows[0].cid);
    });
  }
}

exports.update = (table, data, where, done) => {
  if (!xpool) return done(new Error('Missing database connection.'))

  var keys = Object.keys(data),
    keysValues = keys.map(function(key) { return '`'+ key +'` = '+ ((data[key] == null) ? 'NULL' : '\''+ mysqlEscape(data[key]) +'\'') });

  var keys2 = Object.keys(where),
    whereClause = keys2.map(function(key) { return '`'+ key +'` = \''+ where[key] +'\'' });

  xpool.query('UPDATE `' + table + '` SET '+ keysValues.join(',') +' WHERE '+ whereClause.join(' and '), done);
};

exports.insert = (table, data, done) => {
  if (!xpool) return done(new Error('Missing database connection.'))

  var keys = Object.keys(data),
    values = keys.map(function(key) { return (data[key] == null) ? 'NULL' : "'" + mysqlEscape(data[key]) + "'" });

  xpool.query('INSERT INTO `' + table + '` (' + keys.join(',') + ') VALUES (' + values.join(',') + ')', done);
};

exports.delete = (table, where, done) => {
  if (!xpool) return done(new Error('Missing database connection.'));
  if (where == undefined || !where) return done(new Error('Missing where clause.'));

  var keys = Object.keys(where),
    whereClause = keys.map(function(key) { return '`'+ key +'` = \''+ where[key] +'\'' });

  xpool.query('DELETE FROM `' + table +'` WHERE '+ whereClause.join(' and '), done);
};