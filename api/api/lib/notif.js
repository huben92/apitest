'use strict';

var email    = require('./email'),
	fcm 	 = require('./fcm'),
	db 		 = require('./mysql'),
    config   = require('../config/config'),
	fs 		 = require('fs');

var addCommas = function(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

exports.sendPurchaseNotif = function(user, data) {

	var d = new Date();
	var pd = data.batas_waktu_pembayaran;
	var weekday = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
	const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember"];

	// Send notification to email
	var __hour = pd.getHours();
	if (__hour < 10) __hour = '0' + String(__hour);
	var __minute = pd.getMinutes()
	if (__minute < 10) __minute = '0' + String(__minute);

	db.get('transactions', {id: data.trx_id}, function(err, trxData){
		trxData = trxData[0];
		db.pool().query('SELECT products.*, transaction_items.qty FROM products, transaction_items WHERE products.id = transaction_items.product_id AND transaction_items.trx_id = '+ data.trx_id, function(err, trxProduct){
			if (err){
				console.log(err);
			}

			var emailHtml = 'Hi <b>'+ user.full_name +'</b><br>Checkout berhasil pada tanggal '+ weekday[d.getDay()] +', '+ d.getDate() +' '+ monthNames[d.getMonth()] +' '+ d.getFullYear() +'.<br>Terima kasih atas kepercayaan anda telah berbelanja di Pesan Sembako.<br><br>'+
				'<table><tbody><tr><td style="width: 220px;color: #888;">Total Pembayaran</td>'+
				'<td style="font-weight: bold;">Rp '+ addCommas(data.grand_total) +'</td></tr>'+
				'<tr><td style="color: #888;">Batas Waktu Pembayaran</td><td>'+ weekday[pd.getDay()] +', '+ pd.getDate() +' '+ monthNames[pd.getMonth()] +' '+ pd.getFullYear() +' pukul '+ __hour +':'+ __minute +' WIB</td></tr>'+
				'<tr><td style="color: #888;">Metode Pembayaran</td><td>Transfer</td></tr>'+
				'<tr><td style="color: #888;">Bank</td><td>'+ data.nama_bank +'</td></tr>'+
				'<tr><td style="color: #888;">No. Rekening</td><td><b>'+ data.no_rek +'</b></td></tr>'+
				'<tr><td style="color: #888;">Atas Nama</td><td>'+ data.nama_rek +'</td></tr>'+
				'</tbody></table><br>'+

				'<div style="font-size: 110%; font-weight: bold; margin-bottom: 4px;">Berikut detail pesanan anda:</div>'+
				'<div style="border: #ffa500 1px solid;border-radius: 6px;overflow: hidden;">'+
				'<div style="background-color: #ffa500;">'+
				'<table style="width: 100%;"><thead style="color: #fff; font-weight: bold;"><tr><td style="padding: 8px 12px; width: 50%;"><b>Nama Produk</b></td><td style="padding: 8px 12px; width: 25%;">Qty</td><td style="padding: 8px 12px; width: 25%; text-align: right;">Total Belanja</td></tr></thead></table>'+
				'</div>';

			trxProduct.forEach(function(_trxProduct){
				if (_trxProduct.discount){
					_trxProduct.price_discount = _trxProduct.price;
					if (_trxProduct.discount.indexOf('%') > 0){
						var discount = parseInt(_trxProduct.discount);
						_trxProduct.price = _trxProduct.price_discount - (_trxProduct.price_discount * discount /100);
					} else {
						var discount = parseInt(_trxProduct.discount);
						_trxProduct.price = _trxProduct.price_discount - _trxProduct.discount;
					}
				}

				emailHtml += '<table style="width: 100%;"><tbody>'+
					'<tr><td style="padding: 10px 12px; width: 50%;"><b>'+ _trxProduct.name +'</b></td><td style="padding: 4px 12px; width: 25%;">'+ _trxProduct.qty +' '+ _trxProduct.unit +'</td><td style="padding: 4px 12px; width: 25%; text-align: right;">Rp '+ addCommas(_trxProduct.price * _trxProduct.qty) +'</td></tr>'+
					'</tbody></table>'+
					'<div style="border-top: #ccc 1px solid;"></div>';
			})

			emailHtml += ''+
				'<table style="width: 100%;"><tbody>'+
				'<tr style="color: #888; font-size: 98%;"><td colspan="2" style="padding: 4px 12px; text-align: right; width: 75%;">Subtotal</td><td style="padding: 4px 12px; text-align: right;">Rp '+ addCommas(trxData.total_price) +'</td></tr>';

			if (trxData.discount_ammount > 0){
				emailHtml += '<tr style="color: #888; font-size: 98%;"><td colspan="2" style="padding: 0 12px; text-align: right;">Discount</td><td style="padding: 0 12px; text-align: right;">-Rp '+ addCommas(trxData.discount_ammount) +'</td></tr>';
			}

			emailHtml += ''+
				'<tr style="color: #888; font-size: 98%;"><td colspan="2" style="padding: 0 12px; text-align: right;">Ongkos Kirim</td><td style="padding: 0 12px; text-align: right;">Rp '+ addCommas(trxData.delivery_price) +'</td></tr>'+
				'<tr><td colspan="2" style="padding: 4px 12px; text-align: right; font-size: 110%;">Grand Total</td><td style="padding: 4px 12px; text-align: right; font-size: 110%;"><b>Rp '+ addCommas(trxData.grand_total) +'</b></td></tr>'+
				'</tbody></table>'+
				'</div>';

			emailHtml += ''+
				'<br>Barang akan segera dikirim setelah anda melakukan pembayaran. Pantau status pesanan anda pada tab Transaksi pada aplikasi Pesan Sembako.'+
				'<p style="color: #999;">Demi keamanan transaksi Anda, pastikan untuk tidak menginformasikan bukti dan data pembayaran kepada pihak manapun kecuali Pesan Sembako.</p>';

			email.send('Menunggu Pembayaran Transfer '+ data.nama_bank +' #'+ data.trx_no, emailHtml, user.email, null, function(){});
		})
	})


	var timestamp = Math.floor(Date.now() / 1000);
	var body 	  = 'Mohon segera selesaikan pembayaran anda untuk pembelian sebesar Rp '+ addCommas(data.grand_total) 
		+ ' menggunakan metode pembayaran Transfer ke '+ data.nama_bank +' no. rekening: '+ data.no_rek +' a/n ' + data.nama_rek +'.'
		+ ' \nBatas waktu pembayaran: '+
		weekday[pd.getDay()] +', '+ pd.getDate() +' '+ monthNames[pd.getMonth()] +' '+ pd.getFullYear() +' pukul '+ __hour +':'+ __minute +' WIB';

	var inboxData = {
		type 			: 'notification',
		message_from 	: 'Transaksi #'+ data.trx_no,
		message_to 		: user.id,
		last_message 	: body,
		timestamp 	 	: timestamp,
		trx_id 			: data.trx_id,
		user_id 		: user.id
	}
	db.insert('inbox', inboxData, function(err, result){
		if ( err ) {
			console.log(err);
			return;
		}

		var inboxID = result.insertId;
		var dbData = {
			inbox_id 		: inboxID,
			type 			: 'notification',
			message_from 	: inboxData.message_from,
			message_to 		: inboxData.message_to,
			body 			: body,
			timestamp 		: timestamp
		}
		db.insert('conversations', dbData, function(err, result){
			if (err) console.log(err);
			var convID = result.insertId;
			db.get('conversations', {id: convID}, function(err, conv){
				if (conv && conv.length > 0){

					// Send notification to admin
					// var socket = io.connect(config.uri +':'+ config.port);
					// socket.on('connect', function () {
					// 	socket.emit('sv new purchase');
					// 	socket.close();
					// });

					// Send notification to all device
					db.get('user_devices', {user_id: user.id, type: 'app'}, function(err, devices){
						if ( err ) return;
						devices.forEach(function(device){
							if (device.platform == 'Android' || device.platform == 'android'){
								fcm.send({
									data: {
										type: 'message',
										json: JSON.stringify(conv[0])
									}
								}, [{token: device.uuid}]);
							} else if (device.platform == 'iOS'){
								fcm.sendIOS({
									notification: {
										title: conv[0].message_from,
										body: conv[0].body
									},
									data: {
										type: 'message',
										json: JSON.stringify(conv[0])
									}
								}, [{token: device.uuid}]);
							}
						})
					})
				}
			});
		});
	});
}

exports.paymentReceivedNotif = function(user, data){

	console.log('Sending payment success notification...');

	// Send email notification
	var emailHtml = 'Hi <b>'+ user.full_name +'</b><br>Terima kasih telah menyelesaikan transaksi di '+ config.appName +'. '+
		'Pembayaran anda sudah kami terima dan status pesanan anda sekarang sedang di proses untuk pengiriman.<br><br>'+
		'<table><tbody><tr><td style="width: 220px;color: #888;">Total Pembayaran</td>'+
		'<td style="font-weight: bold;">Rp '+ addCommas(data.grand_total) +'</td></tr>'+
		'<tr><td style="color: #888;">Metode Pembayaran</td><td>Transfer Bank</td></tr>'+
		'</tbody></table><br>'+
		'Pantau status pesanan anda pada tab Transaksi pada applikasi '+ config.appName +
		'<p style="color: #999;">Demi keamanan transaksi Anda, pastikan untuk tidak menginformasikan bukti dan data pembayaran kepada pihak manapun kecuali Pesan Sembako.</p>';

	email.send('Pemabyaran Berhasil Untuk Transaksi #'+ data.trx_no, emailHtml, user.email, null, function(){});


	// Send push notification
	var body = 'Pembayaran sebesar Rp '+ addCommas(data.grand_total) +' untuk transaksi #'+ data.trx_no
		+ ' telah berhasil, status pesanan anda sekarang sedang di proses untuk pengiriman. Kami akan segera memberitahukan ketika barang sudah dikirim beserta no. resi pengiriman.';

	var addConvs = function(inbox, body){
		console.log('Sending message to user...');
		var timestamp = Math.floor(Date.now() / 1000);
		var dbData = {
			inbox_id 		: inbox.id,
			type 			: 'notification',
			message_from 	: inbox.message_from,
			message_to 		: inbox.message_to,
			body 			: body,
			timestamp 		: timestamp
		}

		db.insert('conversations', dbData, function(err, result){
			if (err){
				console.log('Error send purchased notif');
				console.log(err);
			}

			var convID = result.insertId;
			db.get('conversations', {id: convID}, function(err, conv){
				// Send notification to device
				if (conv && conv.length > 0){
					db.get('user_devices', {user_id: user.id, type: 'app'}, function(err, devices){
						if ( err ) return;
						devices.forEach(function(device){
							if (device.platform == 'Android'){
								fcm.send({
									data: {
										type: 'message',
										json: JSON.stringify(conv[0])
									}
								}, [{token: device.uuid}]);
							} else if (device.platform == 'iOS'){
								fcm.sendIOS({
									notification: {
										title: conv[0].message_from,
										body: conv[0].body
									},
									data: {
										type: 'message',
										json: JSON.stringify(conv[0])
									}
								}, [{token: device.uuid}]);
							}
						})
					})
				}
			});
		});
	}

	db.get('inbox', {trx_id: data.trx_id}, function(err, inbox){
		if ( inbox && inbox.length > 0 ){
			// Add conversation to inbox
			inbox.forEach(function(_inbox){
				db.update('inbox', {last_message: body}, {id: _inbox.id}, function(){})
				addConvs(_inbox, body)
			})
		} else {
			// Create new inbox
			var timestamp = Math.floor(Date.now() / 1000);
			var inboxData = {
				type 			: 'notification',
				message_from 	: 'Transaksi #'+ data.trx_no,
				message_to 		: user.id,
				last_message 	: body,
				timestamp 	 	: timestamp,
				trx_id 			: data.trx_id,
				user_id 		: user.id
			}
			db.insert('inbox', inboxData, function(err, result){
				if (err) {
					console.log('Error send notification payment success');
					console.log(err);
					return;
				}
				var inboxID = result.insertId;
				db.get('inbox', {id: inboxID}, function(err, inbox){
					addConvs(inbox[0], body);
				})
			});
		}
	})
}

exports.onDeliveryNotif = function(user, data, courier){
	// Send email notification
	var emailHtml = 'Hi <b>'+ user.full_name +'</b><br>Barang yang anda pesan pada transaksi #'+ data.trx_no +' sudah sudah kami kirimkan. '+
		'Pesanan anda sekarang statusnya Sedang Dalam Pengiriman.<br><br>'+
		'Berikut detail pengiriman untuk pesanan anda:<br>'+
		'<table><tbody><tr><td style="width: 220px;color: #888;">Jasa Pengiriman</td>'+
		'<td style="font-weight: bold;">'+ courier.name +'</td></tr>'+
		'<tr><td style="color: #888;">No. Resi Pengiriman</td><td>'+ data.resi_no +'</td></tr>'+
		'</tbody></table><br>'+
		'Pantau status pesanan anda pada tab Transaksi pada applikasi '+ config.appName +
		'<p style="color: #999;">Demi keamanan transaksi Anda, pastikan untuk tidak menginformasikan bukti dan data pembayaran kepada pihak manapun kecuali Pesan Sembako.</p>';

	email.send('Barang Sedang Dalam Pengiriman Transaksi #'+ data.trx_no, emailHtml, user.email, null, function(){});


	// Send push notification
	var body = 'Barang yang anda pesan pada transaksi #'+ data.trx_no +' sudah sudah kami kirimkan menggunakan jasa pengiriman '+ courier.name +' dengan no. resi pengiriman '+ data.resi_no +'.';

	var addConvs = function(inbox, body){
		console.log('Sending message to user...');
		var timestamp = Math.floor(Date.now() / 1000);
		var dbData = {
			inbox_id 		: inbox.id,
			type 			: 'notification',
			message_from 	: inbox.message_from,
			message_to 		: inbox.message_to,
			body 			: body,
			timestamp 		: timestamp
		}

		db.insert('conversations', dbData, function(err, result){
			if (err){
				console.log('Error send purchased notif');
				console.log(err);
			}

			var convID = result.insertId;
			db.get('conversations', {id: convID}, function(err, conv){
				// Send notification to device
				if (conv && conv.length > 0){
					db.get('user_devices', {user_id: user.id, type: 'app'}, function(err, devices){
						if ( err ) return;
						devices.forEach(function(device){
							if (device.platform == 'Android'){
								fcm.send({
									data: {
										type: 'message',
										json: JSON.stringify(conv[0])
									}
								}, [{token: device.uuid}]);
							} else if (device.platform == 'iOS'){
								fcm.sendIOS({
									notification: {
										title: conv[0].message_from,
										body: conv[0].body
									},
									data: {
										type: 'message',
										json: JSON.stringify(conv[0])
									}
								}, [{token: device.uuid}]);
							}
						})
					})
				}
			});
		});
	}

	db.get('inbox', {trx_id: data.trx_id}, function(err, inbox){
		if ( inbox && inbox.length > 0 ){
			// Add conversation to inbox
			inbox.forEach(function(_inbox){
				db.update('inbox', {last_message: body}, {id: _inbox.id}, function(){})
				addConvs(_inbox, body)
			})
		} else {
			// Create new inbox
			var timestamp = Math.floor(Date.now() / 1000);
			var inboxData = {
				type 			: 'notification',
				message_from 	: 'Transaksi #'+ data.trx_no,
				message_to 		: user.id,
				last_message 	: body,
				timestamp 	 	: timestamp,
				trx_id 			: data.trx_id,
				user_id 		: user.id
			}
			db.insert('inbox', inboxData, function(err, result){
				if (err) {
					console.log('Error send notification payment success');
					console.log(err);
					return;
				}
				var inboxID = result.insertId;
				db.get('inbox', {id: inboxID}, function(err, inbox){
					addConvs(inbox[0], body);
				})
			});
		}
	})
}

exports.deliveredNotif = function(user, data){
	// Send email notification
	var emailHtml = 'Hi <b>'+ user.full_name +'</b><br>Terima kasih telah berbelanja di '+ config.appName +'. Transaksi anda dengan nomor transaksi #'+ data.trx_no +' sudah selesai.<br><br>'+
		'Jika ada masukan, saran, atau keluhan mengenai transaksi ini jangan sungkan untuk menghubungi layanan pelanggan kami di '+ config.CSEmail +' atau melalui telp di '+ config.CSPhone +'.'+
		'<p style="color: #999;">Demi keamanan transaksi Anda, pastikan untuk tidak menginformasikan bukti dan data pembayaran kepada pihak manapun kecuali Pesan Sembako.</p>';

	email.send('Transaksi #'+ data.trx_no +' Sudah Selesai, Terima Kasih Telah Belanja di '+ config.appName, emailHtml, user.email, null, function(){});


	// Send push notification
	var body = 'Terima kasih telah berbelanja di '+ config.appName +'. Transaksi anda dengan nomor transaksi #'+ data.trx_no +' sudah selesai.';

	var addConvs = function(inbox, body){
		console.log('Sending message to user...');
		var timestamp = Math.floor(Date.now() / 1000);
		var dbData = {
			inbox_id 		: inbox.id,
			type 			: 'notification',
			message_from 	: inbox.message_from,
			message_to 		: inbox.message_to,
			body 			: body,
			timestamp 		: timestamp
		}

		db.insert('conversations', dbData, function(err, result){
			if (err){
				console.log('Error send purchased notif');
				console.log(err);
			}

			var convID = result.insertId;
			db.get('conversations', {id: convID}, function(err, conv){
				// Send notification to device
				if (conv && conv.length > 0){
					db.get('user_devices', {user_id: user.id, type: 'app'}, function(err, devices){
						if ( err ) return;
						devices.forEach(function(device){
							if (device.platform == 'Android'){
								fcm.send({
									data: {
										type: 'message',
										json: JSON.stringify(conv[0])
									}
								}, [{token: device.uuid}]);
							} else if (device.platform == 'iOS'){
								fcm.sendIOS({
									notification: {
										title: conv[0].message_from,
										body: conv[0].body
									},
									data: {
										type: 'message',
										json: JSON.stringify(conv[0])
									}
								}, [{token: device.uuid}]);
							}
						})
					})
				}
			});
		});
	}

	db.get('inbox', {trx_id: data.trx_id}, function(err, inbox){
		if ( inbox && inbox.length > 0 ){
			// Add conversation to inbox
			inbox.forEach(function(_inbox){
				db.update('inbox', {last_message: body}, {id: _inbox.id}, function(){})
				addConvs(_inbox, body)
			})
		} else {
			// Create new inbox
			var timestamp = Math.floor(Date.now() / 1000);
			var inboxData = {
				type 			: 'notification',
				message_from 	: 'Transaksi #'+ data.trx_no,
				message_to 		: user.id,
				last_message 	: body,
				timestamp 	 	: timestamp,
				trx_id 			: data.trx_id,
				user_id 		: user.id
			}
			db.insert('inbox', inboxData, function(err, result){
				if (err) {
					console.log('Error send notification payment success');
					console.log(err);
					return;
				}
				var inboxID = result.insertId;
				db.get('inbox', {id: inboxID}, function(err, inbox){
					addConvs(inbox[0], body);
				})
			});
		}
	})
}

exports.sendCCNotif = function(status, user, data){
	db.get('user_devices', {user_id: user.id, type: 'app'}, function(err, devices){
		if ( err ) return;
		devices.forEach(function(device){

			var timestamp = Math.floor(Date.now() / 1000);
			data.cc_payment_status = status;
			fcm.send({
				data: {
					type: 'ccresult',
					json: JSON.stringify(data)
				}
			}, [{token: device.uuid}]);

			fcm.send({
				notification: {
					title: "Pembayaran Kartu Kredit",
					body: "Status pembayaran menggunakan kartu kredit"
				},
				data: {
					type: 'ccresult',
					json: JSON.stringify(data)
				}
			}, [{token: device.uuid}]);
		})
	})
}
