'use strict';

var https    = require('https'),
    config   = require('../config/config');

const sendNotifications = (data, _firebaseKey) => {

    var dataString  = JSON.stringify(data);
    var firebaseKey = 'key='+ _firebaseKey;
    var postOptions = {
        method      : 'POST',
        hostname    : 'fcm.googleapis.com',
        port        : 443,
        path        : '/fcm/send',
        headers     : {
            'Authorization': firebaseKey,
            'Content-Type': 'application/json',
            'Content-Length': dataString.length
        }
    };

    // Set up the request
    var postReq = https.request(postOptions, function(fRes) {
        fRes.setEncoding('utf8');
        var response = '';
        fRes.on('data', function (_response) {
            response += _response;
        });

        fRes.on('end', function() {
            console.log(response);
            // var obj = JSON.parse(response);
        });
    });

    postReq.write(dataString);
    postReq.end();
}

exports.send = (data, regIdArray) => {
    const folds = regIdArray.length % 1000
    for (let i = 0; i < folds; i++) {
        let start = i * 1000,
            end = (i + 1) * 1000

        data['registration_ids'] = regIdArray.slice(start, end).map((item) => {
            return item['token']
        })

        sendNotifications(data, config.firebaseKey)
    }
}

exports.sendIOS = (data, regIdArray) => {
    const folds = regIdArray.length % 1000
    for (let i = 0; i < folds; i++) {
        let start = i * 1000,
            end = (i + 1) * 1000

        data['registration_ids'] = regIdArray.slice(start, end).map((item) => {
            return item['token']
        })

        sendNotifications(data, config.firebaseKeyIOS)
    }
}