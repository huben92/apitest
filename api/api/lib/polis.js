'use strict';

var db 		 = require('./mysql'),
	notif 	 = require('./notif'),
    config   = require('../config/config');

var pad = function(num, n){
	return new Array(n).join('0').slice((n || 2) * -1) + num;
}

var insertPolisDetail = function(polisID, chartItem){
	db.get('chart_item_contacts', {chart_item_id: chartItem.id}, function(err, chartContact){
		var polisContact = chartContact[0];
		delete polisContact.id;
		delete polisContact.chart_item_id;
		polisContact.polis_id = polisID;
		console.log(polisContact.birthdate);
		console.log(typeof polisContact.birthdate);
		if (polisContact.birthdate != null && polisContact.birthdate != '0000-00-00')
			polisContact.birthdate = polisContact.birthdate.toMysqlFormat();
		db.insert('polis_contacts', polisContact, function(err, result){
			db.delete('chart_item', {id: chartItem.id}, function(){});
			db.delete('chart_item_contacts', {chart_item_id: chartItem.id}, function(){});
		});
	});
	db.get('chart_item_insured', {chart_item_id: chartItem.id}, function(err, chartInsured){
		var polisInsured = chartInsured[0];
		delete polisInsured.id;
		delete polisInsured.chart_item_id;
		polisInsured.polis_id = polisID;
		if (polisInsured != null && polisInsured.birthdate != null && typeof polisInsured.birthdate == "object"){
			polisInsured.birthdate = polisInsured.birthdate.toMysqlFormat();
		}
		db.insert('polis_insured', polisInsured, function(err, result){
			db.delete('chart_item_insured', {chart_item_id: chartItem.id}, function(){});
		});
	});
}

exports.generatePolis = function(transaction, next){
	db.get('chart_item', {chart_id: transaction.chart_id}, function(err, chartItems){
		if ( err ) return next('err', null);
		chartItems.forEach(function(chartItem){
			db.get('product_prices', {id: chartItem.product_price_id}, function(err, productPrice){
			if ( err ) return next('err', null);
			db.get('products', {id: productPrice[0].product_id}, function(err, product){
			if ( err ) return next('err', null);
			product = product[0];
			productPrice = productPrice[0];

			var timestamp = Math.floor(Date.now() /1000);
			var d = new Date();
			var vfD = new Date();
			var validFrom = vfD.getFullYear() +'-'+ (vfD.getMonth() +1) +'-'+ vfD.getDate() +' '+ vfD.getHours() +':'+ vfD.getMinutes() +':'+ vfD.getSeconds();
			var validDays = parseInt(productPrice.period);
			vfD.setDate(vfD.getDate() + validDays);
			var validUntil = vfD.getFullYear() +'-'+ (vfD.getMonth() +1) +'-'+ vfD.getDate() +' '+ vfD.getHours() +':'+ vfD.getMinutes() +':'+ vfD.getSeconds();
			var status = (transaction.status == 'paid') ? status = 'aktif' : 'pending';

			var polisData = {
				polis_no 			: 0,
				product_id 			: product.id,
				product_price_id 	: chartItem.product_price_id,
				tipe_pembelian 		: chartItem.type,
				user_id 			: transaction.user_id,
				created_timestamp 	: timestamp,
				transaction_id 		: transaction.id,
				transaction_no 		: 0,
				invoice_no 			: transaction.invoice_no,
				valid_from 			: validFrom,
				valid_until 		: validUntil,
				valid_days 			: validDays,
				payment_type 		: 'sekaligus',
				status 				: status
			}

			db.get('users', {id: transaction.user_id}, function(err, _user){
				if (_user && _user.length > 0){
					db.update('users', {polis_count: _user[0].polis_count+1}, {id: _user[0].id}, function(){})
				}
			})

			db.insert('polis', polisData, function(err, result){
				if ( err ) return next('err', null);
				var polisID = result.insertId;
				var noPolis = '010506' + d.getFullYear().toString().substr(2, 2) + pad(polisID, 6);
				db.update('polis', {polis_no: noPolis, transaction_no: transaction.trx_no}, {id: polisID}, function(){});

				console.log('Insert cart detail: '+ polisID);
				insertPolisDetail(polisID, chartItem);
				next(null, polisID);
			})})});
		})
	});
}