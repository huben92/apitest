'use strict';
module.exports = function(app, _) {

  const jwt = require('jsonwebtoken'),
    config  = require('../../config/config'),
    u       = require('../../controllers/userController');

  if ( _ == undefined ) _ = '/';
  if ( _.slice(-1) != '/' ) _ += '/';

  /**
   * User
   *
   */
  app.route(_+'post').post(u.testPost);
  app.route(_+'get').get(u.testGet);
};
