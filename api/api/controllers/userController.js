'use strict';

const config = require('../config/config'),
    email    = require('../lib/email'),
    multer   = require('multer'),
    fs       = require('fs'),
    util     = require('util'),
    http     = require('http'),
    https    = require('https'),
    md5      = require('md5'),
    jwt      = require('jsonwebtoken'),
    hash     = '__d88ada2_ss2sa++1df3___';

exports.testPost = (req, res) => {
  console.log('POST Request');
  res.json({status: 200});
}

exports.testGet = (req, res) => {
  console.log('GET Request');
  res.json({status: 200});
}
