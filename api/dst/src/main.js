var app = new Vue({
  el: '#app',
  data: {
    nightMode: true,
    isLoading: false,
    hasResult: false,
    uriCopied: false,
    customUri: false,
    uriCopyError: false,

    // Model for input
    inputUri: '',
    inputUriHasError: false,
    inputUriError: '',

    inputCustomUri: '',
    customUriHasError: false,
    customUriError: '',

    resultCode: '',
    qrCode: '',
    visitCount: 0
  },
  methods: {
    changeMode: function() {
      this.nightMode = (!this.nightMode);
      if (this.nightMode){
        document.getElementById("body").classList.add('night');
        document.getElementById("body").classList.remove('day');
      } else {
        document.getElementById("body").classList.add('day');
        document.getElementById("body").classList.remove('night');
      }
    },
    validateUrl: function(value) {
      return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value);
    },
    sendData: function(){
      this.inputUriHasError = false;
      this.customUriHasError = false;

      this.inputUri = this.inputUri.trim();
      if (this.inputUri == ''){
        this.inputUriError = 'Biasain input url dulu, baru klik Biasain.'
        this.inputUriHasError = true;
        return;
      }

      if (this.inputUri.substr(0, 4) != 'http' 
        && this.inputUri.substr(0, 2) != '//'
        && this.inputUri.substr(0, 3) != 'ftp'){
        this.inputUri = 'http://'+this.inputUri;
      }

      if (!this.validateUrl(this.inputUri)){
        this.inputUriError = 'Url yang kamu masukan tidak valid.'
        this.inputUriHasError = true;
        return;
      }

      if (this.customUri){
        if (this.inputCustomUri == ''){
          this.customUriError = 'Kamu belum mengisi custom url.';
          this.customUriHasError = true;
          return;
        }

        if (this.customUri.length > 245){
          this.customUriError = 'Custom url maksimal 245 karakter.';
          this.customUriHasError = true;
          return;
        }
      }

      this.isLoading = true;
      axios.post('/uri/add', {
        uri: this.inputUri,
        custom: this.customUri,
        customUri: this.inputCustomUri
      })
      .then((response) => {
        this.isLoading = false;
        if (response.data.status != 200){
          this.customUriError = response.data.message;
          this.customUriHasError = true;
          return;
        }

        this.inputUri = '';
        this.inputCustomUri = '';
        this.hasResult = true;
        location.replace('/#'+response.data.code);
        this.resultCode = response.data.code;
        this.qrCode = response.data.qr;
      })
      .catch((error) => {
        console.log(error);
        this.isLoading = false;
        this.inputUriError = 'Internal server error.'
        this.inputUriHasError = true;
      });
    },
    goBack: function(){
      this.hasResult = false;
      window.history.replaceState( {} , '', '/' );
    },
    onCopied: function(){
      this.uriCopied = true;
      this.uriCopyError = false;
      setTimeout(() => {
        this.uriCopied = false;
      }, 1500);
    },
    onCopyError: function(){
      this.uriCopied = false;
      this.uriCopyError = true;
      setTimeout(() => {
        this.uriCopyError = false;
      }, 1500);
    },
    toggleCustomUri: function(){
      this.customUri = (!this.customUri);
    }
  },
  mounted: function(){
    const uri = window.location.href.split('#');
    if (uri.length > 1){
      axios.get('/uri/detail/'+uri[1])
      .then(response => {
        if (response.data.status == 200){
          this.hasResult = true;
          this.resultCode = response.data.code;
          this.qrCode = response.data.qr;
          this.visitCount = response.data.visits;
        }
      })
    }
  }
});


setTimeout(function(){
  document.getElementById('loading').classList.add('loadedd');
  setTimeout(function(){
    document.getElementById('body').removeChild(document.getElementById('loading'));
    document.getElementById('formInput').focus();
  }, 300)
}, 0);