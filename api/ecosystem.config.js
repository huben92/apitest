module.exports = {
  apps: [
    {
      name: 'tmfapi',
      script: 'index.js',
      cwd: './',
      exec_mode: 'cluster',
      instances: 2,
      wait_ready: true,
      listen_timeout: 10000,
      kill_timeout: 5000,
      watch: false,
      env: {
        NODE_ENV: 'development'
      },
      env_production: process.env
    }
  ]
}
